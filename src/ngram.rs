use std::ops::Index;
use std::fmt;

const NGRAM_SIZE: usize = 3;

#[derive(PartialEq, Eq, Clone, Copy, PartialOrd, Ord, Hash, Debug)]
pub struct NGram {
    data: [char; NGRAM_SIZE]
}

impl NGram {
    pub fn from(input: &[char]) -> Self {
        return Self { data: [input[0], input[1], input[2]] };
    }
}

impl Index<usize> for NGram {
    type Output = char;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.data[index];
    }
}

impl fmt::Display for NGram {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return write!(f, "({}{}{})", self[0], self[1], self[2]);
    }
}
