mod ngram;

use ngram::NGram;
use std::collections::HashMap;

fn main() {
    let input = "The quick brown fox got shot by the sly farmer!";

    let count_map = {
        let tokens: Vec<char> = input.chars().collect();
        build_profile(&tokens)
    };

    for (k, v) in count_map.iter().filter(|(_, v)| **v > 1) {
        println!("{} = {}", k, v);
    }
}

fn build_profile(tokens: &[char]) -> HashMap<NGram, i32> {
    let mut count_map = HashMap::new();
    for character in tokens.windows(3) {
        count_map
            .entry(NGram::from(character))
            .and_modify(|entry| *entry += 1)
            .or_insert(1);
    }
    return count_map;
}
